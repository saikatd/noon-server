package main

import (
	"database/sql"
	"fmt"
	"strconv"
)

func connectPostgresDb() *sql.DB {
	dbTimeout, err := strconv.Atoi(dbConnTimeout)
	if err != nil {
		logglyLogger.WithField("err", err).Error("Error in converting dbConnTimeout to integer")
		panic(err)
	}
	dbinfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable connect_timeout=%d search_path=%s", dbHost, dbPort, dbUser, dbPassword, dbName, dbTimeout, "chats")
	logglyLogger.Info("Connecting to postgres with ", dbinfo)
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		logglyLogger.WithField("err", err).Error("Error from sq.Open")
		panic(err)
	}
	i, err := strconv.Atoi(dbMaxOpenConns)
	if err != nil {
		logglyLogger.WithField("err", err).Error("Error in converting dbMaxOpenConns to integer")
		panic(err)
	}
	j, err := strconv.Atoi(dbMaxIdleConns)
	if err != nil {
		logglyLogger.WithField("err", err).Error("Error in converting dbMaxIdleConns to integer")
		panic(err)
	}
	db.SetMaxOpenConns(i)
	db.SetMaxIdleConns(j)
	logglyLogger.Info("Setting maxOpenConns to", i, "and maxIdleConns to", j)
	return db
}
