package main

type QueueManager struct {
	topicQueueHolder map[string][]string // topic -> [data1, data2, ....] queue
}

func CreateQueueManager() *QueueManager{
	return &QueueManager{
		topicQueueHolder: make(map[string][]string),
	}
}

func (qManager *QueueManager) PushToQueue(topic, data string) {
	if _, ok := qManager.topicQueueHolder[topic]; !ok {
		qManager.topicQueueHolder[topic] = []string{data}
	} else {
		qManager.topicQueueHolder[topic] = append(qManager.topicQueueHolder[topic], data)
	}
}
