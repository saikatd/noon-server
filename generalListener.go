package main

import (
	"bitbucket.org/mechmocha/golang-mmutils"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"time"
)

type GeneralMessage struct {
	Topic string `json:"topic"`
	Data string `json:"data"`
	SocketId string `json:"socket_id"`
	Type string `json:"type"`
}

type GeneralListener struct {
	socketManager     *SocketManager
	qManager          *QueueManager
	redisClientPubSub *mmutils.RedisClients
	logger            *logrus.Entry
}

func CreateGeneralListener(sockManager *SocketManager, qManager *QueueManager, redisClientPubSub  *mmutils.RedisClients, logger *logrus.Entry) *GeneralListener{
	return &GeneralListener{
		socketManager:     sockManager,
		qManager:          qManager,
		redisClientPubSub: redisClientPubSub,
		logger:            logger,
	}
}

func (listener *GeneralListener) handleMessage(msg GeneralMessage) {
	if msg.Type == REGISTER {
		listener.socketManager.RegisterSocket(msg.Topic, msg.SocketId)
	} else if msg.Type == PUBLISH {
		listener.qManager.PushToQueue(msg.Topic, msg.Data)
		listener.socketManager.PublishToSockets(msg.Topic, msg.Data)
	}
}

func (listener *GeneralListener) Subscribe(){
	gPubSubConn, onClose, err := listener.redisClientPubSub.Subscribe(generalTopic)
	if err != nil {
		listener.logger.WithField("err", err).Error("error during subscribing to ", generalTopic)
	}
	defer onClose()
	for {
		switch v := gPubSubConn.ReceiveWithTimeout(1000*time.Second).(type) {
		case redis.Message:
			var message GeneralMessage
			dec := json.NewDecoder(bytes.NewBuffer(v.Data))
			dec.UseNumber()
			if err := dec.Decode(&message); err != nil {
				listener.logger.Error("error during unmarshalling of received message with error being ", err)
				panic(err)
			}
			listener.logger.Info("a message is received at general_topic", message)
			listener.handleMessage(message)
		case redis.Subscription:
			listener.logger.Debug(fmt.Sprintf("message type is subscription %s: %s %d\n", v.Channel, v.Kind, v.Count))
		case error:
			listener.logger.WithField("topic", generalTopic).Debug("timeout reached in pubsub conn")
			return
		}
	}
}