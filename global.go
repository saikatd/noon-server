package main

import "os"

var(
	listenAddress                         = os.Getenv("LISTEN_ADDRESS")
	logglyToken                           = os.Getenv("LOGGLY_TOKEN")
	logglyHost               			  = os.Getenv("LOGGLY_HOST")
	logglyTag                			  = os.Getenv("LOGGLY_TAG")
	dbHost                                = os.Getenv("DB_HOST")
	dbPort                                = os.Getenv("DB_PORT")
	dbUser                                = os.Getenv("DB_USER")
	dbPassword                            = os.Getenv("DB_PASSWORD")
	dbName                                = os.Getenv("DB_NAME")
	dbMaxOpenConns                        = os.Getenv("DB_MAX_OPEN_CONNECTIONS")
	dbMaxIdleConns                        = os.Getenv("DB_MAX_IDLE_CONNECTIONS")
	dbConnTimeout                         = os.Getenv("DB_CONNECTION_TIMEOUT")
	redisMaxIdleConns                     = os.Getenv("REDIS_MAX_IDLE_CONNECTION")
	redisMaxActiveConns                   = os.Getenv("REDIS_MAX_ACTIVE_CONNECTION")
	redisListenAddress                    = os.Getenv("REDIS_LISTEN_ADDRESS")
	redisPubSubMaxIdleConns               = os.Getenv("REDIS_PUB_SUB_MAX_IDLE_CONNECTION")
	redisPubSubMaxActiveConns             = os.Getenv("REDIS_PUB_SUB_MAX_ACTIVE_CONNECTION")
	redisPubSubListenAddress              = os.Getenv("REDIS_PUB_SUB_LISTEN_ADDRESS")
	generalTopic						  = os.Getenv("GENERAL_TOPIC")
)