package mmutils

import (
	"errors"
	"strconv"
	"github.com/Sirupsen/logrus"
	redis "github.com/garyburd/redigo/redis"
)

func RedisConnectionPool(redisMaxIdleConns, redisMaxActiveConns, redisListenAddress string, dbNumber int, logglyLogger *logrus.Entry) *redis.Pool {
	idleConns, _ := strconv.Atoi(redisMaxIdleConns)
	activeConns, _ := strconv.Atoi(redisMaxActiveConns)
	return &redis.Pool{
		MaxIdle:   idleConns,
		MaxActive: activeConns,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", redisListenAddress)
			if err != nil {
				logglyLogger.WithField("err", err).Error("error in redis.Dial")
				return nil, err
			}
			_, err = c.Do("SELECT", dbNumber)
			if err != nil {
				logglyLogger.WithField("err", err).Error("error in redis db select")
				return nil, err
			}
			return c, err
		},
		Wait: true,
	}
}

type RedisClient struct {
	connPool *redis.Pool
	isPubSub bool
}

type RedisClients struct {
	connPools []*redis.Pool
	numberOfShards int
	isPubSub bool
}

func NewRedisClient(isPubSubMode bool, redisMaxIdleConns, redisMaxActiveConns, redisListenAddress string, dbNumber int, logglyLogger *logrus.Entry) *RedisClient {
	redisClient := RedisClient{
		isPubSub: isPubSubMode,
		connPool: RedisConnectionPool(redisMaxIdleConns, redisMaxActiveConns, redisListenAddress, dbNumber, logglyLogger),
	}
	return &redisClient
}

func RedisShardedClientsForPubSub(redisMaxIdleConns,redisMaxActiveConns string, redisListenAddresses []string, dbNumber int, logglyLogger *logrus.Entry) *RedisClients {
	redisClients := RedisClients{
		isPubSub:true,
		numberOfShards:len(redisListenAddresses),
		connPools: make([]*redis.Pool,len(redisListenAddresses)),
	}
	for i := 0; i< len(redisListenAddresses); i++ {
		redisClients.connPools[i] = RedisConnectionPool(redisMaxIdleConns, redisMaxActiveConns, redisListenAddresses[i], dbNumber, logglyLogger)
	}

	return &redisClients
}


func (r *RedisClients) Publish(channelName string, data interface{}) (interface{}, error) {
	if !r.isPubSub {
		return nil, errors.New("publishing is not allowed in non-pub-sub mode")
	}
	shardnumber := int(HashingStringToNumberUsingFnv(channelName)) % r.numberOfShards
	client := r.connPools[shardnumber].Get()
	subscribers, err := client.Do("PUBLISH", channelName, data)
	client.Close()
	return subscribers, err
}

// onClose function needs to be called once the subscriber has closed
// dev takes care of closing PubSubConn
func (r *RedisClients) Subscribe(channelName string) (subscriber *redis.PubSubConn, onClose func(), err error) {
	if !r.isPubSub {
		return nil, func() {}, errors.New("subscribing is not allowed in non-pub-sub mode")
	}
	shardnumber := int(HashingStringToNumberUsingFnv(channelName)) % r.numberOfShards
	client := r.connPools[shardnumber].Get()
	gPubSubConn := &redis.PubSubConn{Conn: client}
	_err := gPubSubConn.Subscribe(channelName)
	onclose := func() {
		gPubSubConn.Close()
		client.Close()
	}
	return gPubSubConn, onclose, _err
}

func (r *RedisClient) SetEx(key string, duration int, value interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("SetEx is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("SETEX", key, duration, value)
	client.Close()
	return result, err
}
func (r *RedisClient) Expire(key string, duration int) ( reply interface{},err error) {
	if r.isPubSub {
		return nil,errors.New("Expire is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("Expire", key, duration)
	client.Close()
	return result,err
}

func (r *RedisClient) Incr(key string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Incr is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("Incr", key)
	client.Close()
	return result, err
}

func (r *RedisClient) IncrBy(key string, value int) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("IncrBy is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("INCRBY", key, value)
	client.Close()
	return result, err
}

func (r *RedisClient) Set(key string, value interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Set is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("SET", key, value)
	client.Close()
	return result, err
}

func (r *RedisClient) Get(key string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Get is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("GET", key)
	client.Close()
	return result, err
}

func (r *RedisClient) Exists(key string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Exists is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("EXISTS", key)
	client.Close()
	return result, err
}

func (r *RedisClient) HExists(hashKey,key string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Exists is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("HEXISTS",hashKey, key)
	client.Close()
	return result, err
}

func (r *RedisClient) Del(key string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Del is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	result, err := client.Do("DEL", key)
	client.Close()
	return result, err
}

func (r *RedisClient) LRange(key string, startIdx, stopIdx int) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Lrange is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	vals, err := client.Do("LRANGE", key, startIdx, stopIdx)
	client.Close()
	return vals, err
}

func (r *RedisClient) RPush(key, element interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Rpush is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("RPUSH", key, element)
	client.Close()
	return val, err
}

func (r *RedisClient) LPush(key, element interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Rpush is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("LPUSH", key, element)
	client.Close()
	return val, err
}

func (r *RedisClient) LTrim(key string, startIdx, stopIdx int) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Rpush is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("LTRIM", key, startIdx, stopIdx)
	client.Close()
	return val, err
}

func (r *RedisClient) LRem(key, element string, count int) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Lrem is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("LREM", key, count, element)
	client.Close()
	return val, err
}

// this expects key1, value1, key2, value2 for hash hashkey
func (r *RedisClient) HmSet(args ...interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("Lrange is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("HMSET", args...)
	client.Close()
	return val, err
}

func (r *RedisClient) HmGet(args ...interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("HmGet is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	vals, err := client.Do("HMGET", args...)
	client.Close()
	return vals, err
}

func PrependingKey(key interface{} ,args ...interface{}) []interface{} {
	intrimHashKey := make([]interface{}, 1)
	intrimHashKey[0] = key
	return append(intrimHashKey, args...)
}

func (r *RedisClient) HGet(hashKey string, field string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("HmGet is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	vals, err := client.Do("HGET", hashKey, field)
	client.Close()
	return vals, err
}

func (r *RedisClient) HSet(hashKey string, field string, value interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("HSet is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("HSET", hashKey, field, value)
	client.Close()
	return val, err
}

func (r *RedisClient) HGetAll(hashKey string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("HmGetAll is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	vals, err := client.Do("HGETALL", hashKey)
	client.Close()
	return vals, err
}

func (r *RedisClient) HDel(hashKey string, arg string) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("HDel is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("HDEL", hashKey, arg)
	client.Close()
	return val, err
}

func (r *RedisClient) MGet(args ...interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("MGet is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("MGET", args...)
	client.Close()
	return val, err
}

func (r *RedisClient) MGetWithArray(args []interface{}) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("MGetWithArray is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("MGET", args...)
	client.Close()
	return val, err
}

func (r *RedisClient) BLPop(key string, timeout int) (reply interface{}, err error) {
	if r.isPubSub {
		return nil, errors.New("BLPop is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	val, err := client.Do("BLPOP", key, timeout)
	client.Close()
	return val, err
}


func (r *RedisClient) MULTI() (redis.Conn, error) {
	if r.isPubSub {
		return  nil, errors.New("MULTI is not allowed in pub-sub mode")
	}
	client := r.connPool.Get()
	client.Send("MULTI")
	return  client, nil
}

func (r *RedisClient) SEND(redisConn redis.Conn,key string, args ...interface{}) error {
	if r.isPubSub {
		return  errors.New("SEND is not allowed in pub-sub mode")
	}
	redisConn.Send(key, args...)
	return  nil
}

func (r *RedisClient) DISCARD(redisConn redis.Conn) error {
	if r.isPubSub {
		return  errors.New("DISCARD is not allowed in pub-sub mode")
	}
	err := redisConn.Send("DISCARD")
	redisConn.Close()
	return err
}

func (r *RedisClient) EXEC(redisConn redis.Conn) error {
	if r.isPubSub {
		return  errors.New("EXEC is not allowed in pub-sub mode")
	}
	_, err := redisConn.Do("EXEC")
	redisConn.Close()
	return err
}