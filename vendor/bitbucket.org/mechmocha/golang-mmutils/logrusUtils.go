package mmutils

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
)

func SetupLogglyLogger(logglyToken, logglyHost, logglyTag, appName, appVersion string) (*logrus.Entry, func()) {
	ls := logrus.New()
	ls.Level = logrus.DebugLevel
	ls.Formatter = &logrus.JSONFormatter{}
	retFn := func() {}
	if logglyTag != "DEV" {
		//Removing loggly hook
		/*h := logrusly.NewLogglyHook(logglyToken, logglyHost, logrus.InfoLevel, logglyTag)
		ls.Hooks.Add(h)
		retFn = func() {
			h.Flush()
		}*/
	}
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	host := strings.Split(hostname, ".")
	logglyLogger := ls.WithFields(logrus.Fields{
		"hostname":   host[0],
		"app":        appName,
		"appVersion": appVersion,
	})
	return logglyLogger, retFn
}

func GenerateUuidV4() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	uuid := fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	return strings.ToLower(uuid), nil
}

func CreateRequestLogger(rq *http.Request, logglyLogger *logrus.Entry) *logrus.Entry {
	requestID := rq.Header.Get("X-Request-ID")
	requestLogger := logglyLogger
	if requestID == "" {
		requestUUID, err := GenerateUuidV4()
		if err != nil {
			logglyLogger.WithField("err", err).Error("Could not create V4 UUID")
			panic(err)
		}
		return requestLogger.WithFields(logrus.Fields{"rid": requestUUID, "ridFromUser": false, "endpoint": rq.URL.Path})
	}
	return requestLogger.WithFields(logrus.Fields{"rid": requestID, "ridFromUser": true, "endpoint": rq.URL.Path})
}

func CreateCustomRequestLogger(requestID string, endpoint string, logglyLogger *logrus.Entry) *logrus.Entry {
	if requestID == "" {
		requestUUID, err := GenerateUuidV4()
		if err != nil {
			logglyLogger.WithField("err", err).Error("Could not create V4 UUID")
			panic(err)
		}
		return logglyLogger.WithFields(logrus.Fields{"rid": requestUUID, "endpoint": endpoint})
	}
	return logglyLogger.WithFields(logrus.Fields{"rid": requestID, "endpoint": endpoint})
}

type myResponseWriter struct {
	statusCode           int
	bytesWritten         int
	responsePayloadBytes []byte
	http.ResponseWriter
}

func (m *myResponseWriter) Write(payloadBytes []byte) (int, error) {
	sz, err := m.ResponseWriter.Write(payloadBytes)
	if err != nil {
		m.bytesWritten = -1
	} else {
		m.bytesWritten = sz
	}
	m.responsePayloadBytes = append(m.responsePayloadBytes, payloadBytes...)
	return sz, err
}

func (m *myResponseWriter) WriteHeader(statusCode int) {
	m.statusCode = statusCode
	m.ResponseWriter.WriteHeader(statusCode)
}

func ResponseReportingMiddleware(logglyLogger *logrus.Entry) func(http.Handler) http.Handler {
	return func(wrapped http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, rq *http.Request) {
			if rq.Header.Get("X-Request-ID") != "" {
				rw.Header().Add("X-Request-ID", rq.Header.Get("X-Request-ID"))
			} else {
				rid, _ := GenerateUuidV4()
				rq.Header.Set("X-Request-ID", rid)
			}

			requestLogger := CreateRequestLogger(rq, logglyLogger)
			xTrace := (rq.Header.Get("X-Trace") == "TRUE")
			writer := myResponseWriter{200, -1, []byte{}, rw}

			requestPayloadBytes, err := ioutil.ReadAll(rq.Body)
			if err != nil {
				requestLogger.WithField("err", err).Error("Error in reading request body")
			} else {
				rq.Body = ioutil.NopCloser(bytes.NewReader(requestPayloadBytes))
				if xTrace {
					requestLogger.WithFields(logrus.Fields{"referrer": rq.UserAgent(), "contentLength": rq.ContentLength,
						"requestPayload": string(requestPayloadBytes)}).Info("Enter Handler")
				} else {
					requestLogger.WithFields(logrus.Fields{"referrer": rq.UserAgent(), "contentLength": rq.ContentLength,
						"requestPayload": string(requestPayloadBytes)}).Debug("Enter Handler")
				}
			}

			startTime := time.Now().UnixNano()
			wrapped.ServeHTTP(&writer, rq)
			responseTime := (time.Now().UnixNano() - startTime) / 1000
			
			if xTrace {
				requestLogger.WithFields(logrus.Fields{"statusCode": writer.statusCode, "bytesWritten": writer.bytesWritten,
					"responseTime": responseTime, "responsePayload": string(writer.responsePayloadBytes)}).Info("Exit Handler")
			} else {
				requestLogger.WithFields(logrus.Fields{"statusCode": writer.statusCode, "bytesWritten": writer.bytesWritten,
					"responseTime": responseTime, "responsePayload": string(writer.responsePayloadBytes)}).Debug("Exit Handler")
			}
		})
	}
}

func LogErrorOnDbPrepare(requestLogger *logrus.Entry, err error, query string) {
	requestLogger.WithField("err", err).Error("Error in db.Prepare for ", query)
}
func LogErrorOnDbQuery(requestLogger *logrus.Entry, err error, query string) {
	requestLogger.WithField("err", err).Error("Error in db.Query for ", query)
}
func LogErrorOnDbExec(requestLogger *logrus.Entry, err error, query string) {
	requestLogger.WithField("err", err).Error("Error in db.Exec for ", query)
}
