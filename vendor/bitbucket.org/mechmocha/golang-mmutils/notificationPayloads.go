package mmutils

import (
	"encoding/json"
	"github.com/Sirupsen/logrus"
	"strings"
)

func CreateGcmAndIosNotification(title, subtitle, ticker, smallIcon, trackingId, isUpdateNotification, notificationId string,
	isSilent bool, placementId int, tag, notificationType string, androidPayload, iosPayload interface{}, requestLogger *logrus.Entry) (string, string) {
	type androidNotification struct {
		Title                string      `json:"title"`
		Subtitle             string      `json:"subtitle"`
		Ticker               string      `json:"ticker"`
		SmallIcon            string      `json:"smallIcon"`
		TrackingId           string      `json:"trackingID"`
		IsUpdateNotification string      `json:"isUpdateNotification"`
		NotificationId       string      `json:"notificationID"`
		IsSilent             bool        `json:"is_silent"`
		PlacementId          int         `json:"placementid"`
		Tag                  string      `json:"tag"`
		NotificationType     string      `json:"notificationType"`
		NotificationPayload  interface{} `json:"notificationPayload"`
	}

	type iosNotification struct {
		Alert            string `json:"alert"`
		Badge            int    `json:"badge"`
		Sound            string `json:"sound"`
		ContentAvailable int    `json:"content_available"`
		MutableContent   int    `json:"mutable-content"`
	}

	androidData := androidNotification{
		title,
		subtitle,
		ticker,
		smallIcon,
		trackingId,
		isUpdateNotification,
		notificationId,
		isSilent,
		placementId,
		tag,
		notificationType,
		androidPayload,
	}
	var iosData iosNotification
	subtitle = strings.Replace(subtitle, ":", "", -1)
	if !isSilent {
		iosData = iosNotification{
			subtitle,
			1,
			"default",
			0,
			1,
		}
	} else {
		iosData = iosNotification{
			subtitle,
			1,
			"default",
			1,
			1,
		}
	}

	android := make(map[string]interface{})
	ios := make(map[string]interface{})
	android["data"] = androidData
	ios["aps"] = iosData
	ios["iosPayload"] = iosPayload

	androidBytes, err := json.Marshal(android)
	if err != nil {
		requestLogger.WithField("err", err).Error("json marshalling error in createNotification")
		panic(err)
	}
	iosBytes, err := json.Marshal(ios)
	if err != nil {
		requestLogger.WithField("err", err).Error("json marshalling error in createNotification")
		panic(err)
	}

	return string(androidBytes), string(iosBytes)
}
