package mmutils

import (
	"bytes"
	"encoding/gob"
	"github.com/Sirupsen/logrus"
)

func GobEncode(val interface{}, requestLogger *logrus.Entry) []byte {
	b := new(bytes.Buffer)
	enc := gob.NewEncoder(b)
	if err := enc.Encode(val); err != nil {
		requestLogger.WithField("err", err).Error("error in gob encoding val interface")
	} else {
		return b.Bytes()
	}
	return nil
}
