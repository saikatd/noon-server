package mmutils

import (
	"context"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/pkg/errors"

	kafka "github.com/segmentio/kafka-go"
)

type KafkaWriter struct {
	writer *kafka.Writer
	config kafka.WriterConfig
}
type DefaultBalancer struct {
}

const UNDERSCORE string = "_"

func (db *DefaultBalancer) Balance(msg kafka.Message, partitions ...int) int {
	return partitions[0]
}

func GetKafkaWriter(kafkaBrokerUrls []string, topic string, balancer kafka.Balancer, environment string) *KafkaWriter {
	dialer := &kafka.Dialer{
		Timeout:  10 * time.Second,
		ClientID: "utils",
	}

	config := kafka.WriterConfig{
		Brokers:      kafkaBrokerUrls,
		Topic:        environment + UNDERSCORE + topic,
		Balancer:     balancer,
		Dialer:       dialer,
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
	}
	w := kafka.NewWriter(config)
	return &KafkaWriter{writer: w, config: config}
}

// GetKafkaWriterV2 creates and returns a *KafkaWriter
func GetKafkaWriterV2(kafkaBrokerUrls []string, topic string, balancer kafka.Balancer, environment string, batchSize int) *KafkaWriter {
	dialer := &kafka.Dialer{
		Timeout:  10 * time.Second,
		ClientID: "utils",
	}

	if batchSize < 0 {
		batchSize = 0 // 0 is automatically overidden to default value by kafka-go. Adding this check as we are not sending any error
	}

	config := kafka.WriterConfig{
		Brokers:      kafkaBrokerUrls,
		Topic:        environment + UNDERSCORE + topic,
		Balancer:     balancer,
		Dialer:       dialer,
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
		BatchSize:    batchSize,
	}
	w := kafka.NewWriter(config)
	return &KafkaWriter{writer: w, config: config}
}

func (kw *KafkaWriter) Push(message []byte, key string) error {
	if key == "" {
		return errors.New("kafka partition key cannot be empty")
	}
	if message == nil {
		return errors.New("kafka message cannot be nil")
	}
	parent := context.Background()
	value := kafka.Message{
		Key:   []byte(key),
		Value: message,
		Time:  time.Now(),
	}
	return kw.writer.WriteMessages(parent, value)
}

func (kw *KafkaWriter) Close() error {
	return kw.writer.Close()
}

func (kw *KafkaWriter) GetConfig() kafka.WriterConfig {
	return kw.config
}

func (kw *KafkaWriter) GetStats() kafka.WriterStats {
	return kw.writer.Stats()
}

type KafkaReader struct {
	reader *kafka.Reader
}

func GetKafkaReader(brokers []string, topic, groupID string, environment string) *KafkaReader {
	config := kafka.ReaderConfig{
		Brokers:         brokers,
		Topic:           environment + UNDERSCORE + topic,
		GroupID:         groupID,
		MinBytes:        10e3,            // 10KB
		MaxBytes:        10e6,            // 10MB
		MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
		ReadLagInterval: -1,
	}

	r := kafka.NewReader(config)
	return &KafkaReader{reader: r}
}

func (kr *KafkaReader) Read() (kafka.Message, error) {
	parent := context.Background()
	return kr.reader.ReadMessage(parent)
}

func (kr *KafkaReader) Close() error {
	return kr.reader.Close()
}

func (kr *KafkaReader) GetStats() kafka.ReaderStats {
	return kr.reader.Stats()
}

func CreateKafkaMessageLogger(message kafka.Message, logglyLogger *logrus.Entry) *logrus.Entry {
	requestId, err := GenerateUuidV4()
	if err != nil {
		logglyLogger.WithField("err", err).Error("Could not create V4 UUID")
		requestId = "unknown"
	}
	return logglyLogger.WithFields(logrus.Fields{
		"rid":          requestId,
		"request_type": "kafka",
		"topic":        message.Topic,
		"key":          message.Key,
	})
}

func CreateKafkaWriter(brokerUrls []string, topic string, kafkaEnvironment string) *KafkaWriter {
	kwriter := GetKafkaWriterV2(brokerUrls, topic, &kafka.Hash{}, kafkaEnvironment, 1)
	return kwriter
}

func (kafkaWriter *KafkaWriter) PublishMessage(messageBytes []byte, key string, customLogger *logrus.Entry) {
	if err := kafkaWriter.Push(messageBytes, key); err != nil {
		customLogger.WithField("err", err).WithField("topic", kafkaWriter.config.Topic).Error("there was an error during writing of message on kafka")
		return
	}
	customLogger.Info("message was successfully written on kafka message: ", string(messageBytes), " kafka info:", kafkaWriter.GetConfig())
}


