package mmutils

import (
	"context"
	"crypto/tls"

	"github.com/google/uuid"

	"github.com/Sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	GrpcRequestIdKey = "grpc_request_id"
	RequestIdKey     = "rid"
)

// This is an experimental API
func unaryClientInterceptor(ctx context.Context,
	method string, req, reply interface{},
	cc *grpc.ClientConn, invoker grpc.UnaryInvoker,
	opts ...grpc.CallOption) error {
	return nil
}

func unaryServerInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Errorf(codes.InvalidArgument, "Error retreiving metadata")
	}
	requestIds, ok := md[GrpcRequestIdKey]
	var requestId string
	if !ok {
		requestId = uuid.New().String()
	} else {
		requestId = requestIds[0]
	}
	newctx := context.WithValue(ctx, GrpcRequestIdKey, requestId)
	return handler(newctx, req)
}

func NewGrpcClient(listenAddress string, requestLogger *logrus.Entry) (*grpc.ClientConn, error) {
	cc, err := grpc.Dial(listenAddress, grpc.WithInsecure())
	if err != nil {
		requestLogger.WithField("err", err).Error("Could not connect to server at ", listenAddress)
		return nil, err
	}
	return cc, nil
}

func NewGrpcClientWithContext(listenAddress string, requestLogger *logrus.Entry) (*grpc.ClientConn, context.Context, error) {
	cc, err := grpc.Dial(listenAddress, grpc.WithInsecure())
	if err != nil {
		requestLogger.WithField("err", err).Error("Could not connect to server at ", listenAddress)
		return nil, nil, err
	}
	requestId := ""
	if val, ok := requestLogger.Data[RequestIdKey]; ok {
		requestId = val.(string)
	}
	ctx := context.Background()
	if requestId != "" {
		ctx = metadata.AppendToOutgoingContext(context.Background(), GrpcRequestIdKey, requestId)
	}
	return cc, ctx, nil
}

func NewGrpcServer() *grpc.Server {
	server := grpc.NewServer(grpc.UnaryInterceptor(unaryServerInterceptor))
	return server
}

func NewSecureGrpcClient(listenAddress string, requestLogger *logrus.Entry) *grpc.ClientConn {
	creds := credentials.NewTLS(&tls.Config{
		//TLS Config values here
	})

	conn, err := grpc.Dial(listenAddress, grpc.WithTransportCredentials(creds))
	if err != nil {
		requestLogger.WithField("err", err).Error("Could not connect to server at ", listenAddress)
	}
	return conn
}

func NewSecureGrpcServer() *grpc.Server {
	creds := credentials.NewTLS(&tls.Config{
		// TLS config values here
	})

	credServerOption := grpc.Creds(creds)
	server := grpc.NewServer(credServerOption, grpc.UnaryInterceptor(unaryServerInterceptor))
	return server
}

func CreateGrpcRequestLogger(ctx context.Context, logger *logrus.Entry) *logrus.Entry {
	requestId := ctx.Value(GrpcRequestIdKey)
	if requestId == "" {
		newRid, err := GenerateUuidV4()
		if err != nil {
			logger.WithField("err", err).Error("Could not create V4 UUID")
			newRid = "unknown"
		}
		requestId = newRid
	}
	return logger.WithFields(logrus.Fields{
		RequestIdKey: requestId,
	})
}
