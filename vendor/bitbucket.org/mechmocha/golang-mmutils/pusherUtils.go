package mmutils

import (
	"github.com/golang/protobuf/proto"
	"encoding/json"
	"github.com/Sirupsen/logrus"
	pusher_message "bitbucket.org/mechmocha/proto-common/kafka-topics/pusher-message"
)

type ClientChatMessage struct {
	SuperType string      `json:"super_type"`
	Type      string      `json:"type"`
	Data      interface{} `json:"data"`
}

const (
	PRESENCE_SUPER_TYPE = "PRESENCE"
)

func BuildPusherPayload(superType string ,messageType string, payload interface{}, customLogger *logrus.Entry) []byte{
	if superType == " " || superType == "" {
		superType = PRESENCE_SUPER_TYPE
	}
	clientChatMessage := ClientChatMessage{
		SuperType: superType,
		Type:      messageType,
		Data:      payload,
	}

	clientChatMessageBytes, err := json.Marshal(&clientChatMessage)
	if err != nil {
		customLogger.WithField("error", err).Error("error while marshalling clientChatMessage")
		return nil
	}
	return clientChatMessageBytes
}

func BuildPusherMessage(toPlayerId, fromPlayerId, messageType string, payload interface{}, isPnEnabled bool, ackLevel int, customLogger *logrus.Entry) pusher_message.PusherMessage {
	pusherMessage := pusher_message.PusherMessage{
		ToPlayerId:toPlayerId,
		FromPlayerId:fromPlayerId,
		MessageType:messageType,
		JsonPayload:BuildPusherPayload(PRESENCE_SUPER_TYPE,messageType, payload, customLogger),
		IsPushNotificationEnabled:isPnEnabled,
		AckLevel:int64(ackLevel),
	}
	return pusherMessage
}

func BuildPusherMessageAndSend(toPlayerId, fromPlayerId, messageType, offlinePusherType string, payload interface{}, isPushNotificationEnabled, isForcePushNotification bool,
	ackLevel int, logger *logrus.Entry, pusherWriter *KafkaWriter) {
	messageId, err := GenerateUuidV4()
	if err != nil {
		logger.WithField("err", err).Error("Error while generating UUID for message Id in pusher message")
		return
	}
	pusherMessage := pusher_message.PusherMessage{
		ToPlayerId:toPlayerId,
		FromPlayerId:fromPlayerId,
		JsonPayload:BuildPusherPayload(PRESENCE_SUPER_TYPE, messageType, payload, logger),
		MessageId:messageId,
		OfflinePusherType:offlinePusherType,
		IsPushNotificationEnabled:isPushNotificationEnabled,
		ForcePushNotification:isForcePushNotification,
		AckLevel:int64(ackLevel),
		MessageType:messageType,
	}

	MarshalAndSendMessageToPusher(&pusherMessage, pusherWriter, logger)
}

func BuildPusherMessageAndSend_V2(toPlayerId, fromPlayerId, messageType,superType string, offlinePusherType string, payload interface{}, isPushNotificationEnabled, isForcePushNotification bool,
	ackLevel int, logger *logrus.Entry, pusherWriter *KafkaWriter) {
	messageId, err := GenerateUuidV4()
	if err != nil {
		logger.WithField("err", err).Error("Error while generating UUID for message Id in pusher message")
		return
	}
	pusherMessage := pusher_message.PusherMessage{
		ToPlayerId:toPlayerId,
		FromPlayerId:fromPlayerId,
		JsonPayload:BuildPusherPayload(superType, messageType, payload, logger),
		MessageId:messageId,
		OfflinePusherType:offlinePusherType,
		IsPushNotificationEnabled:isPushNotificationEnabled,
		ForcePushNotification:isForcePushNotification,
		AckLevel:int64(ackLevel),
		MessageType:messageType,
	}
	
	MarshalAndSendMessageToPusher(&pusherMessage, pusherWriter, logger)
}
func MarshalAndSendMessageToPusher(pusherMessage *pusher_message.PusherMessage, pusherWriter *KafkaWriter, logger *logrus.Entry) {
	pusherMessageBytes, err := proto.Marshal(pusherMessage)
	if err  != nil {
		logger.WithField("err", err).Error("Error while marshalling of pusher message")
		return
	}
	pusherWriter.PublishMessage(pusherMessageBytes, pusherMessage.ToPlayerId, logger)
}