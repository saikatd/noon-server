package mmutils

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base32"
	"encoding/hex"
	"hash/fnv"
)

func ConvertToSha256(value string) string {
	hash := sha256.New()
	hash.Write([]byte(value))
	return hex.EncodeToString(hash.Sum(nil))
}

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func GenerateRandomString(s int) (string, error) {
	b, err := generateRandomBytes(s)
	return hex.EncodeToString(b), err
}

func HashingStringToNumberUsingFnv(key string) uint32 {
	hash := fnv.New32a()
	hash.Write([]byte(key))
	return hash.Sum32()
}

func GenerateRandomStringBase32(s int) (string, error) {
	b, err := generateRandomBytes((s * 5)/8 + 1)
	if err != nil {
		return "", err
	}
	enc := base32.NewEncoding("abcdefghijklmnopqrstuvwxyz123456")
	return ("z"+enc.EncodeToString(b)[:(s-1)]), nil
}
