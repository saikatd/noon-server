package mmutils

func Int64Min(x, y int64) int64 {
	if x < y {
		return x
	}
	return y
}

func Int64Max(x, y int64) int64 {
	if x > y {
		return x
	}
	return y
}

func IntMin(x, y int) int {
	return int(Int64Min(int64(x), int64(y)))
}

func IntMax(x, y int) int {
	return int(Int64Max(int64(x), int64(y)))
}
