package mmutils

import (
	"errors"

	"github.com/Sirupsen/logrus"
)

func Encrypt(toBeEncrypted string, key string, requestLogger *logrus.Entry) string {
	requestLogger.WithFields(logrus.Fields{
		"toBeEncrypted": toBeEncrypted,
		"key":           key}).Debug("starting Custom encryption")
	keyLength := len(key)
	moduloNumber := 256
	checksumModulo := 16

	offset := 0
	for _, r := range key {
		offset += int(r)
	}

	positionalOffset := 0
	if keyLength > 1 {
		positionalOffset = offset - int(key[keyLength-1])
	} else {
		positionalOffset = offset
	}
	//offset corrections
	offset %= moduloNumber
	positionalOffset %= moduloNumber

	checkSumFirstHalf := 0
	cipheredValue := ""
	charSet := []rune(toBeEncrypted)
	for i, r := range charSet {
		asciiValue := int(r)
		checkSumFirstHalf += (asciiValue * i) % checksumModulo
		cipheredValue += string((asciiValue + offset + positionalOffset*i) % moduloNumber)
	}
	checkSumFirstHalf %= checksumModulo
	checkSumSecondHalf := checkSumFirstHalf

	checksum := (checkSumFirstHalf << 4) + checkSumSecondHalf
	cipher := cipheredValue + string(checksum)
	requestLogger.WithFields(logrus.Fields{
		"encrypted": cipher}).Debug("successfully completed encryption")
	return cipher
}

func Decrypt(toBeDecrypted string, key string, requestLogger *logrus.Entry) (string, error) {
	requestLogger.WithFields(logrus.Fields{
		"toBeDecrypted": toBeDecrypted,
		"key":           key}).Debug("starting decryption")
	keyLength := len(key)
	moduloNumber := 256
	checksumModulo := 16

	offset := 0
	for _, r := range key {
		offset += int(r)
	}

	positionalOffset := 0
	if keyLength > 1 {
		positionalOffset = offset - int(key[keyLength-1])
	} else {
		positionalOffset = offset
	}
	//offset corrections
	offset %= moduloNumber
	positionalOffset %= moduloNumber

	checkSumFirstHalf := 0
	decipheredValue := ""
	charSet := []rune(toBeDecrypted)
	for i, r := range charSet[:len(charSet)-1] {
		decipheredAscii := int(r) - offset - positionalOffset*i
		if decipheredAscii < 0 {
			decipheredAscii = moduloNumber + (decipheredAscii % moduloNumber)
		}
		checkSumFirstHalf += (decipheredAscii * i) % checksumModulo
		decipheredValue += string(decipheredAscii)
	}
	checkSumFirstHalf %= checksumModulo
	checkSumSecondHalf := checkSumFirstHalf
	sourceChecksum := int(charSet[len(charSet)-1])
	computedChecksum := (checkSumFirstHalf << 4) + checkSumSecondHalf

	if sourceChecksum == computedChecksum {
		requestLogger.WithFields(logrus.Fields{
			"decryptedValue": decipheredValue,
		}).Debug("Successfully decrypted")
		return decipheredValue, nil
	} else {
		requestLogger.WithFields(logrus.Fields{
			"sourceChecksum":   sourceChecksum,
			"computedChecksum": computedChecksum,
			"decryptedValue":   decipheredValue,
		}).Error("checksum did not match")
		return "", errors.New("checksum did not match")
	}
}
