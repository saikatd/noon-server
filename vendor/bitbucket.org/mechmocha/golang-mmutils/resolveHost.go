package mmutils

import (
	"github.com/Sirupsen/logrus"
	"os"
	"strings"
)

func GetConfigBasedOnHostname(requestLogger *logrus.Entry) (int, int, string) {
	hostName, err := os.Hostname()
	if err != nil {
		requestLogger.WithField("err", err).Error("error in getting hostname")
	}
	host := strings.Split(hostName, ".")
	switch host[0] {
	case "ip-172-31-95-224": //staging
		return 2, 0, host[0]
	case "ip-172-31-254-11": //staging
		return 2, 1, host[0]
	case "ip-172-31-81-222": //prod
		return 2, 0, host[0]
	case "ip-172-31-245-73": //prod
		return 2, 1, host[0]
	case "sandbox":
		return 1, 0, host[0]
	default:
		return -1, -1, ""
	}
}
