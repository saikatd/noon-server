package mmutils

import (
	"encoding/json"
	"net/http"
)

func HTTPFailWith4xx(errorCode string, httpStatusCode int, rw http.ResponseWriter) {
	responseJson, err := json.Marshal(struct {
		Success   bool   `json:"success"`
		ErrorCode string `json:"error_code"`
	}{
		false,
		errorCode,
	})
	if err != nil {
		panic(err)
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(httpStatusCode)
	rw.Write(responseJson)
}

func HTTPSuccessWith200(data interface{}, rw http.ResponseWriter) {
	responseJson, err := json.Marshal(struct {
		Success bool        `json:"success"`
		Data    interface{} `json:"data"`
	}{
		true,
		data,
	})
	if err != nil {
		panic(err)
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.Write(responseJson)
}
