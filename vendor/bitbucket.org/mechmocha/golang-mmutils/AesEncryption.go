package mmutils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"

	"github.com/Sirupsen/logrus"
)

func AesEncrypt(plaintext string, requestLogger *logrus.Entry) (string, error) {
	key := []byte(GenerateKey())
	requestLogger.WithFields(logrus.Fields{
		"toBeEncrypted": plaintext,
		"key":           key}).Debug("starting AES encryption")
	plaintextBytes := PKCS5Padding([]byte(plaintext))
	if len(plaintextBytes)%aes.BlockSize != 0 {
		return "", errors.New("plaintext is not a multiple of the block size")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	ciphertext := make([]byte, len(plaintextBytes))
	iv := key

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext, plaintextBytes)

	cipherText := base64.StdEncoding.EncodeToString(ciphertext)
	requestLogger.WithFields(logrus.Fields{
		"ciphertext": cipherText}).Debug("successfully completed encryption")
	return cipherText, nil
}

func AesDecrypt(cipheredText string, requestLogger *logrus.Entry) (string, error) {
	key := []byte(GenerateKey())
	requestLogger.WithFields(logrus.Fields{
		"cipheredText": cipheredText,
		"key":          key}).Debug("starting AES decryption")
	ciphertext, err := base64.StdEncoding.DecodeString(cipheredText)
	if err != nil {
		return "", err
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	if len(ciphertext) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}
	iv := key

	if len(ciphertext)%aes.BlockSize != 0 {
		return "", errors.New("ciphertext is not a multiple of the block size")
	}
	mode := cipher.NewCBCDecrypter(block, iv)

	mode.CryptBlocks(ciphertext, ciphertext)

	plaintext := string(PKCS5UnPadding(ciphertext))
	requestLogger.WithFields(logrus.Fields{
		"plaintext": plaintext}).Debug("successfully completed decryption")
	return plaintext, nil
}

func AesEncryptWithKey(plaintext string, Encryptkey string, iv string,requestLogger *logrus.Entry) (string, error) {
	key := []byte(Encryptkey)
	//iv should be of 16 chars
	ivBytes := []byte(iv)
	requestLogger.WithFields(logrus.Fields{
		"toBeEncrypted": plaintext,
		"key":           key}).Debug("starting AES encryption")
	plaintextBytes := PKCS5Padding([]byte(plaintext))
	if len(plaintextBytes)%aes.BlockSize != 0 {
		return "", errors.New("plaintext is not a multiple of the block size")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	ciphertext := make([]byte, len(plaintextBytes))


	mode := cipher.NewCBCEncrypter(block, ivBytes)
	mode.CryptBlocks(ciphertext, plaintextBytes)

	cipherText := base64.StdEncoding.EncodeToString(ciphertext)
	requestLogger.WithFields(logrus.Fields{
		"ciphertext": cipherText}).Debug("successfully completed encryption")
	return cipherText, nil
}

func AesDecryptWithKey(cipheredText string,Decryptkey string,iv string,requestLogger *logrus.Entry) (string, error) {
	key := []byte(Decryptkey)
	//iv should be of 16 chars
	ivBytes := []byte(iv)
	requestLogger.WithFields(logrus.Fields{
		"cipheredText": cipheredText,
		"key":          key}).Debug("starting AES decryption")
	ciphertext, err := base64.StdEncoding.DecodeString(cipheredText)
	if err != nil {
		return "", err
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	if len(ciphertext) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}
	if len(ciphertext)%aes.BlockSize != 0 {
		return "", errors.New("ciphertext is not a multiple of the block size")
	}
	mode := cipher.NewCBCDecrypter(block, ivBytes)
	mode.CryptBlocks(ciphertext, ciphertext)
	plaintext := string(PKCS5UnPadding(ciphertext))
	requestLogger.WithFields(logrus.Fields{
		"plaintext": plaintext}).Debug("successfully completed decryption")
	return plaintext, nil
}

func PKCS5Padding(src []byte) []byte {
	padding := aes.BlockSize - len(src)%aes.BlockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(src, padtext...)
}

func PKCS5UnPadding(src []byte) []byte {
	length := len(src)
	unpadding := int(src[length-1])
	return src[:(length - unpadding)]
}

func GenerateKey() string {
	start := "54^GF9=_yt~>}-"
	taste := "#$"
	start += taste
	return start
}
