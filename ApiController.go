package main

import (
	"database/sql"
	"github.com/Sirupsen/logrus"
)

type ApiController struct {
	db *sql.DB
	logger *logrus.Entry
}

func InitApiController(db *sql.DB, logger *logrus.Entry) *ApiController{
	return &ApiController{
		db: db,
		logger: logger,
	}
}

