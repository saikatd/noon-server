package main
import(
	"bitbucket.org/mechmocha/golang-mmutils"
	"encoding/json"
	"fmt"
	"github.com/Sirupsen/logrus"
)

type SocketManager struct {
	topicSocketManager map[string][]string // mapping topic -> [sock1, sock2]
	socketTopicMapping map[string][]string // reverse mapping socket -> [topic1, topic2]
	socketOffsetMgr map[string]int // mapping socket_id -> offset(for a given topic)
	redisClientPubSub  *mmutils.RedisClients
	logger *logrus.Entry
}

func CreateTopicManager(redisClientPubSub  *mmutils.RedisClients, logger *logrus.Entry) *SocketManager {
	return &SocketManager{
		topicSocketManager: make(map[string][]string),
		redisClientPubSub: redisClientPubSub,
		logger: logger,
	}
}

type PublishMessage struct {
	Data string `json:"data"`
}

func (topicMgr *SocketManager) RegisterSocket(topic, socket string) {
	if _, ok := topicMgr.topicSocketManager[topic]; ok {
		topicMgr.topicSocketManager[topic] = append(topicMgr.topicSocketManager[topic], socket)
	} else {
		topicMgr.topicSocketManager[topic] = []string{socket}
	}
}

func (topicMgr *SocketManager) PublishToSockets(topic, data string) {
	fmt.Println("before publishing topic:", topic, " and data:",data)
	pubMessage := PublishMessage{Data:data}
	marshalledPayload, err := json.Marshal(pubMessage)
	if err != nil {
		topicMgr.logger.WithField("err", err).Error("error during marshalling of close subscriber message")
	}
	if _, ok := topicMgr.topicSocketManager[topic]; ok {
		fmt.Println("mapping of topic to socket_id", topicMgr.topicSocketManager)
		fmt.Println("about to publish message:", string(marshalledPayload))
		for _, socket := range	topicMgr.topicSocketManager[topic] {
			_, err = topicMgr.redisClientPubSub.Publish(socket, marshalledPayload)
			if err != nil {
				topicMgr.logger.WithField("err", err).Error("there was an error during publishing of message to socketId", socket)
			}
			topicMgr.logger.WithField("socket_id", socket).Info("publishing message on socket_id::", data)
		}
	}

}