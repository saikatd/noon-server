package main

import (
	//"net/http"
	`github.com/Sirupsen/logrus`
	"bitbucket.org/mechmocha/golang-mmutils"
	"strings"
)

var (
	logglyLogger *logrus.Entry
	serviceName  = "noon-server"
)

func main() {
	var logglyFlusher func()
	logglyLogger, logglyFlusher = mmutils.SetupLogglyLogger(logglyToken, logglyHost, logglyTag, serviceName, "1.0")
	//db := connectPostgresDb()
	//InitApiController(db, logglyLogger)
	shards := strings.Split(redisPubSubListenAddress, ",")
	redisClientPubSub := mmutils.RedisShardedClientsForPubSub(redisPubSubMaxIdleConns, redisPubSubMaxActiveConns, shards, 0, logglyLogger)
	qManager := CreateQueueManager()
	topicManager := CreateTopicManager(redisClientPubSub, logglyLogger)
	listener := CreateGeneralListener(topicManager, qManager, redisClientPubSub, logglyLogger)
	listener.Subscribe()
	//if err := http.ListenAndServe(listenAddress, nil); err != nil {
	//	logglyLogger.WithField("err", err).Error("webserver shutdown")
	//}
	logglyFlusher()
}